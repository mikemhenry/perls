"""Define the project's workflow logic and operation functions.

Execute this script directly from the command line, to view your project's
status, execute operations and submit them to a cluster. See also:

    $ python src/project.py --help
"""
from flow import FlowProject, directives
from flow.environment import DefaultSlurmEnvironment


class MyProject(FlowProject):
    pass


class Kestrel(DefaultSlurmEnvironment):
    hostname_pattern = "kestrel"
    template = "kestrel.sh"

    @classmethod
    def add_args(cls, parser):
        parser.add_argument(
            "--partition", default="batch", help="Specify the partition to submit to."
        )


# Definition of project-related labels (classification)
def current_step(job):
    import gsd.hoomd

    if job.isfile("trajectory.gsd"):
        with gsd.hoomd.open(job.fn("trajectory.gsd")) as traj:
            return traj[-1].configuration.step
    return -1


# @MyProject.label
# def progress(job):
#    return '{}/4'.format(int(round(current_step(job) / job.doc.steps) * 4))


@MyProject.label
def sampled(job):
    return current_step(job) >= job.doc.steps


@MyProject.label
def initialized(job):
    return job.isfile("init.gsd")


# Adding project operations


@directives(executable="python -u")
@MyProject.operation
@MyProject.post(initialized)
def initialize(job):
    import os
    import hoomd
    import itertools
    import logging
    import numpy as np

    np.random.seed(job.sp.seed)
    unique_types_int = np.arange(job.sp.sigma_min, job.sp.sigma_max + 1)
    unique_types_str = list(map(str, unique_types_int))
    sigmas = unique_types_int / 10
    params = dict(zip(unique_types_str, sigmas))

    if hoomd.context.exec_conf is None:
        hoomd.context.initialize("")
    with job:
        with hoomd.context.SimulationContext():
            if os.path.isfile("init.gsd"):
                logging.info("File exits, skipping init")
                return
            # Change distributions here
            # shift s from 0,1 to 10,30
            s = np.random.power(job.sp.a, job.sp.N) * 20 + 10
            counts, bins = np.histogram(s, bins=np.arange(10, 30 + 1), range=(10, 30))
            counts = counts[::-1]
            types = list(
                itertools.chain.from_iterable(
                    map(lambda b, c: [str(b)] * int(c), bins, counts)
                )
            )

            # Just the types that actually make it into the simulation
            type_list = list(set(types))
            typeid_map = dict(zip(type_list, range(0, len(type_list))))
            snap = hoomd.data.make_snapshot(
                N=job.sp.N, box=hoomd.data.boxdim(L=job.sp.L), particle_types=type_list
            )

            positions = set()
            while len(positions) < job.sp.N:
                p = tuple(
                    np.random.randint(-job.sp.L / 2, job.sp.L / 2) for _ in range(3)
                )
                if p not in positions and all([r % 3 == 0 for r in p]):
                    positions.add(p)

            snap.particles.position[:] = np.array(list(positions))
            snap.particles.diameter[:] = [params[_] for _ in types]
            snap.particles.mass[:] = [params[_] ** 3 for _ in types]
            snap.particles.typeid[:] = [typeid_map[_] for _ in types]
            hoomd.init.read_snapshot(snap)
            hoomd.dump.gsd("init.gsd", period=None, group=hoomd.group.all())


@directives(executable="python -u")
@directives(ngpu=1)
@MyProject.operation
@MyProject.pre.after(initialize)
@MyProject.post(sampled)
def sample(job):
    import hoomd
    import hoomd.md
    import itertools
    import logging
    import numpy as np

    np.random.seed(job.sp.seed)
    unique_types_int = np.arange(job.sp.sigma_min, job.sp.sigma_max + 1)
    unique_types_str = list(map(str, unique_types_int))
    sigmas = unique_types_int / 10
    params = dict(zip(unique_types_str, sigmas))

    if hoomd.context.exec_conf is None:
        hoomd.context.initialize("--mode=gpu")
    with job:
        with hoomd.context.SimulationContext():
            hoomd.init.read_gsd("init.gsd", restart="restart.gsd")
            nl = hoomd.md.nlist.tree()

            lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)
            lj.set_params(mode="xplor")

            hoomd.util.quiet_status()
            logging.info("Setting coefs")
            for par0, par1 in itertools.combinations_with_replacement(params, 2):
                combined_sigma = (params[par0] + params[par1]) / 2
                lj.pair_coeff.set(
                    par0,
                    par1,
                    epsilon=1,
                    sigma=combined_sigma,
                    r_cut=combined_sigma * 2.5,
                )
            logging.info("Done setting coefs")
            hoomd.util.unquiet_status()

            hoomd.md.integrate.mode_standard(dt=job.sp.dt)
            all = hoomd.group.all()
            hoomd.md.integrate.langevin(group=all, kT=job.sp.kT, seed=job.sp.seed)
            hoomd.dump.gsd(
                "trajectory.gsd", period=1e6, group=all, overwrite=False, phase=0
            )
            hoomd.analyze.log(
                "dump.log",
                [
                    "volume",
                    "momentum",
                    "pair_lj_energy",
                    "potential_energy",
                    "kinetic_energy",
                    "pressure",
                    "temperature",
                ],
                1e5,
                phase=0,
            )
            gsd_restart = hoomd.dump.gsd(
                "restart.gsd", period=1e6, group=all, truncate=True, phase=0
            )

            try:
                hoomd.run_upto(job.doc.steps + 1, limit_multiple=1e6)
            except hoomd.WalltimeLimitReached:
                pass
            finally:
                gsd_restart.write_restart()
                job.document["sample_step"] = hoomd.get_step()


if __name__ == "__main__":
    MyProject().main()
