#!/usr/bin/env python
"""Initialize the project's data space.

Iterates over all defined state points and initializes
the associated job workspace directories."""
import signac
import logging


def main():
    project = signac.init_project("kT-Sweep")
    for kT in [0.5, 1.0, 1.5, 2.0, 3.0]:
        statepoint = dict(
            N=10_000, L=200, sigma_min=10, sigma_max=30, seed=42, kT=kT, dt=0.001, a=1
        )
        job = project.open_job(statepoint)
        job.doc.setdefault("steps", int(1e6))
        job.doc.steps += int(1e7)
        job.init()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
